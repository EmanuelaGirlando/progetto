import math


class Point:
    def __init__(self, x: float, y: float):
        self.x=x   
        self.y=y



class IPolygon:
    def area(self) -> float:
        return 0.0



class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        self.center=center
        self.a=a
        self.b=b
    def area(self):
        return self.a * self.b * math.pi



class Circle(Ellipse):
    def __init__(self, center: Point, radius: int):
        Ellipse.__init__(self, center,radius,radius)
    def area  (self):
        return Ellipse.area(self)


class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self.p1=p1
        self.p2=p2
        self.p3=p3
        pass
    def area (self):
        latoA= math.sqrt((self.p2.x-self.p1.x)*(self.p2.x-self.p1.x)+ (self.p2.y-self.p1.y)*(self.p2.y-self.p1.y))
        latoB= math.sqrt((self.p2.x-self.p3.x)*(self.p2.x-self.p3.x)+ (self.p2.y-self.p3.y)*(self.p2.y-self.p3.y))
        latoC=math.sqrt((self.p3.x-self.p1.x)*(self.p3.x-self.p1.x)+ (self.p3.y-self.p1.y)*(self.p3.y-self.p1.y))
        p=(latoA+latoB+latoC)/2
        return math.sqrt(p*(p-latoC)*(p-latoB)*(p-latoA))



class TriangleEquilateral(Triangle):
    def __init__(self, p1: Point, edge: int):
        Triangle.__init__(self, p1, Point(p1.x+edge,p1.y),Point(p1.x+edge/2,p1.y+math.sqrt(3)*edge/2))

    def area(self):
        return Triangle.area(self)



class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self.p1=p1
        self.p2=p2
        self.p3=p3
        self.p4=p4
    def area (self):
        return (self.p1.x*self.p2.y+self.p2.x*self.p3.y+self.p3.x*self.p4.y+self.p4.x*self.p1.y-self.p1.y*self.p2.x-self.p2.y*self.p3.x-self.p3.y*self.p4.x-self.p4.y*self.p1.x)*1/2


class Parallelogram(Quadrilateral):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        Quadrilateral.__init__(self,p1,  p2, Point(p2.x + p4.x -p1.x,p4.y+p2.y-p1.y),p4)
    def area(self):
        return Quadrilateral.area(self)



class Rectangle(Parallelogram):
    def __init__(self, p1: Point, base: int, height: int):
        Parallelogram.__init__(self,p1,Point(p1.x+base,p1.y), Point (p1.x,p1.y+height))
    def area(self):
        return Parallelogram.area(self)



class Square(Rectangle):
    def __init__(self, p1: Point, edge: int):
        Rectangle.__init__(self,p1,edge,edge)
    def area(self):
       return Rectangle.area(self)

