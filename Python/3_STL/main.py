class Ingredient:
    def __init__(self, name: str, price: int, description: str):
        self.Name = name
        self.Price = price
        self.Description = description

class Pizza:
    def __init__(self, name: str):
        self.Name = name
        self.ingredients = []

    def addIngredient(self, ingredient: Ingredient):
        sorted(self.ingredients)
        self.ingredients.append(ingredient)

    def numIngredients(self) -> int:
        return len(self.ingredients)

    def computePrice(self) -> int:
        prezzo: int = 0
        for ingredient in self.ingredients:
            prezzo += ingredient.Price
        return prezzo

class Order:
    def __init__(self, name: str):
        self.Name = name
        self.pizzas = []
        self.numOrder = 0

    def getPizza(self, position: int) -> Pizza:  #data una posizione restituisce la pizza presente
        numPizzas = self.numPizzas()
        if position < 0 or position > numPizzas:  #valore position è possibile ?
            raise Exception("Position passed is wrong")
        else:
            return self.pizzas[position]

    def initializeOrder(self, numPizzas: int):
        self.pizzas = [] * numPizzas

    def addPizza(self, pizza: Pizza):
        self.pizzas.append(pizza)

    def numPizzas(self) -> int:
        return len(self.pizzas)

    def computeTotal(self) -> int:  # somma costo totale delle pizze
        prezzo = 0
        for pizza in self.pizzas:
            prezzo += pizza.computePrice()
        return prezzo


class Pizzeria:

    def __init__(self):
        self.numOrders = 1000
        self.orders = []  # lista degli ordini
        self.dictionaryIngred = {}  # DIZIONARIO STRING INGREDIENT
        self.dictionaryPizza = {}  # dizionario string Pizza
        self.pizzas = []

        self.ingredients = []

    def addIngredient(self, name: str, description: str, price: int):
        if self.dictionaryIngred.get(name) is None:
            ingredient = Ingredient(name, price, description)
            self.dictionaryIngred[name] = ingredient

        else:
            raise Exception("Ingredient already inserted")

    def findIngredient(self, name: str) -> Ingredient:
        ingredient = self.dictionaryIngred.get(name)
        if ingredient is not None:
            return ingredient
        else:
            raise Exception("Ingredient not found")


    def addPizza(self, name: str, ingredients: []):
        pizza = self.dictionaryPizza.get(name)
        if pizza is not None:  # se c'è, errore
            raise Exception("Pizza already inserted")
        else:
            new_pizza = Pizza(name)
            new_pizza.Name = name
            for ingredName in ingredients:
                foundIngredient = self.findIngredient(ingredName)
                new_pizza.addIngredient(foundIngredient)
            self.dictionaryPizza[name] = new_pizza


    def findPizza(self, name: str) -> Pizza:
        pizza = self.dictionaryPizza.get(name)

        if pizza is not None:
            return pizza
        else:
            raise Exception("Pizza not found")

    def createOrder(self, pizzas: []) -> int:

        if len(pizzas) == 0:
            raise Exception("Empty order")
        else:
            order = Order(pizzas)
            order.initializeOrder(len(pizzas))
            for i in range(0, len(pizzas)):
                pizza = self.findPizza(pizzas[i])
                order.addPizza(pizza)
            self.orders.append(order)
            self.numOrders = self.numOrders + 1
            return self.numOrders - 1

    def findOrder(self, numOrder: int) -> Order:
        if (numOrder < 1000 or numOrder > self.numOrders - 1):
            raise Exception("Order not found")
        else:
            return self.orders[numOrder - 1000]

    def getReceipt(self, numOrder: int) -> str:
        order = self.findOrder(numOrder)
        prezzo = str()
        scontrino = ""
        totale = ""
        for i in range(0, order.numPizzas()):
            pizza = order.getPizza(i)
            prezzo = str(pizza.computePrice())
            scontrino += "- " + pizza.Name + ", " + prezzo + " euro" + "\n"

        totale = str(order.computeTotal())
        scontrino += " TOTAL: " + totale + " euro" + "\n"
        return scontrino

    def listIngredients(self) -> str:
        stampa = ""
        prezzo = None

        for ingredient in self.dictionaryIngred:
            ingrediente = self.dictionaryIngred[ingredient]
            prezzo = str(ingrediente.Price)
            stampa += (ingrediente.Name + " - '" + ingrediente.Description + "': " + prezzo + " euro" + "\n")

        return stampa

    def menu(self) -> str:
        menu = ""
        num_ingredienti = ""
        prezzo = ""

        for pizza in self.dictionaryPizza:
            pizza1 = self.dictionaryPizza[pizza] #salvo la pizza corrispondende alla chiave
            prezzo = str(pizza1.computePrice())
            print(pizza1)
            num_ingredienti = str(pizza1.numIngredients())
            menu += pizza1.Name + " (" + num_ingredienti + " ingredients): " + prezzo + " euro" + "\n"
        return menu