@startuml
interface ICutter
interface IPolygon

ICutter <|-- ConvexCutter
ICutter <|-- ConcaveCutter
IPolygon <|-- Polygon

class Intersector {
-double toleranceParallelism
-double toleranceIntersection
-Type type
-Position positionIntersectionCutter
-Position positionIntersectionEdge
-Vector2d resultParametricCoordinates
-Point originCutter//damodificare    
-Vector2d rightHandSide 
-Matrix2d matrixTangentVector
--
+Intersector()
+SetToleraceparallelism()
+SetCutter()
+setEdge()
}

class Point {
int punto//inserirenamespaceovunque
-Vectord2d(coordinate) 
double GetX()
double GetY()
 doubleComputeNorm2()
}


note left of Point::counter
  Add Namespace
end note

class Vertex {
 int id   
-Point(point) 
+Vertex()
const Point GetPoint()
double GetX ()
double GetY()
}

class Segment {
-point(from)
-point(to)
--
+Segment()
Segment( const  Point da, const Point a)
Point GetFrom()
Point GetTo()
}
enum Type {
  NoIntersection
  IntersectionOnLine
  IntersectionOnSegment 
  IntersectionParallelOnLine 
  IntersectionParallelOnSegment 
}
enum Position {
  Begin
  Inner
  End 
  Outer
}
@enduml
