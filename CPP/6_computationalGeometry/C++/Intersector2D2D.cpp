#include "Intersector2D2D.h"

// ***************************************************************************
Intersector2D2D::Intersector2D2D()
{
toleranceIntersection=1.0E-7;
toleranceParallelism=1.0E-5;
}


Intersector2D2D::~Intersector2D2D()
{

}
// ***************************************************************************
void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNomalVector.row(0)=planeNormal;
    tangentLine(0)=planeTranslation;
    return;
}

// ***************************************************************************
void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNomalVector.row(1)=planeNormal;
    tangentLine(1)=planeTranslation;
    return ;
}

// ***************************************************************************
bool Intersector2D2D::ComputeIntersection()
{
    Vector3d riga1;
    Vector3d riga2;

    riga1=matrixNomalVector.row(0);
    riga2=matrixNomalVector.row(1);

    double x,y,z;
    x= riga1.y()*riga2.z()-riga1.z()*riga2.y();
    y=riga2.x()*riga1.z()-riga1.x()*riga2.z();
    z=riga1.x()*riga2.y()-riga1.y()*riga2.x();

    Vector3d prodottoVett=Vector3d(x,y,z);
    matrixNomalVector.row(2)=prodottoVett;
    tangentLine(2)=0;
    if (prodottoVett.x()>-toleranceIntersection && prodottoVett.x()<toleranceIntersection && prodottoVett.y()>-toleranceIntersection && prodottoVett.y()<toleranceIntersection &&prodottoVett.z()>-toleranceIntersection && prodottoVett.z()<toleranceIntersection)
    {
        double controllo=tangentLine(0)-tangentLine(1);
        if (controllo>-toleranceParallelism && controllo < toleranceParallelism)
            intersectionType=Coplanar;
        else
            intersectionType=NoInteresection;
        return false;
    }
    intersectionType=LineIntersection;
    return true;
}
