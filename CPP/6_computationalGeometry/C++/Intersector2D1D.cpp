#include "Intersector2D1D.h"

// ***************************************************************************
Intersector2D1D::Intersector2D1D()
{
    toleranceParallelism = 1.0E-7;
    toleranceIntersection = 1.0E-7;

}
Intersector2D1D::~Intersector2D1D()
{

}

Vector3d Intersector2D1D::IntersectionPoint()
{
    double x,y,z;
    x=lineOriginPointer->x() + intersectionParametricCoordinate*lineTangentPointer->x();
    y=lineOriginPointer->y() + intersectionParametricCoordinate*lineTangentPointer->y();
    z=lineOriginPointer->z() + intersectionParametricCoordinate*lineTangentPointer->z();
    return Vector3d(x,y,z);
}

// ***************************************************************************
void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    planeNormalPointer=&planeNormal;
    planeTranslationPointer=&planeTranslation;
    return;
}
// ***************************************************************************
void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{
    lineOriginPointer=&lineOrigin;
    lineTangentPointer=&lineTangent;
    return;
}
// ***************************************************************************
bool Intersector2D1D::ComputeIntersection()
{
    double den,num;
    den=planeNormalPointer->x()*lineTangentPointer->x() + planeNormalPointer->y()*lineTangentPointer->y() + planeNormalPointer->z()*lineTangentPointer->z();
    num=*planeTranslationPointer-(planeNormalPointer->x()*lineOriginPointer->x() + planeNormalPointer->y()*lineOriginPointer->y() + planeNormalPointer->z()*lineOriginPointer->z());
    if (den<-toleranceIntersection || den> toleranceIntersection)
    {
        intersectionType=PointIntersection;
        intersectionParametricCoordinate=num/den;
        return true;
    }
    else
    {
        if (num > -toleranceParallelism && num <toleranceParallelism)
            intersectionType=Coplanar;
        else
            intersectionType=NoInteresection;
    }
    return  false;
}


