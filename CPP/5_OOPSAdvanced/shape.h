#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <vector>
#include <math.h>
using namespace std;

namespace ShapeLibrary {

  class Point {
    public:
      double X;
      double Y;
      Point();
      Point(const double& x,
            const double& y);
      Point(const Point& point);

      double ComputeNorm2() const { return sqrt(X*X+ Y*Y); }

      Point operator+(const Point& point) const;
      Point operator-(const Point& point) const;
      Point& operator-=(const Point& point);  //implementati nel cpp
      Point& operator+=(const Point& point);


      friend ostream& operator<<(ostream& stream, const Point& point)
      {
          stream << "Point: x = "<< point.X<< " y = "<<point.Y;
          return stream;
      }
  };

  class IPolygon {
    public:
      virtual double Perimeter() const = 0;
      virtual void AddVertex(const Point& point) = 0;
      friend inline bool operator< (const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter() < rhs.Perimeter(); }
      friend inline bool operator> (const IPolygon& lhs, const IPolygon& rhs){ return rhs.Perimeter() < lhs.Perimeter(); }
      friend inline bool operator<=(const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter() <= rhs.Perimeter();}
      friend inline bool operator>=(const IPolygon& lhs, const IPolygon& rhs){ return rhs.Perimeter() <= lhs.Perimeter(); }

  };

  class Ellipse : public IPolygon
  {
    protected:
      Point _center;
      double _a;
      double _b;

    public:
      Ellipse();
      Ellipse(const Point& center,
              const double& a,
              const double& b);
      virtual ~Ellipse() { }
      void AddVertex(const Point& point) { _center=point;}
      double Perimeter() const;
  };

  class Circle : public Ellipse
  {
    public:
      Circle() { Ellipse(); }
      Circle(const Point& center,
             const double& radius);
      virtual ~Circle() { }
      double Perimeter() const;

  };


  class Triangle : public IPolygon
  {
    protected:
      vector<Point> points;

    public:
      Triangle() {
          points.reserve(3);
          for (int i=0;i<3;i++)
              points[i]=Point();
      }
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);

      void AddVertex(const Point& point) {
          points[0]=points[1];
          points[1]=points[2];
          points[2]=point;
      }
      double Perimeter() const;
  };


  class TriangleEquilateral : public Triangle
  {
    public:
      TriangleEquilateral(const Point& p1,
                          const double& edge);
      double perimeter() const;
  };

  class Quadrilateral : public IPolygon
  {
    protected:
      vector<Point> points;

    public:
      Quadrilateral() {
          points.resize(4);
          for (int i=0;i<4;i++)
              points[i]=Point();

      }
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);
      virtual ~Quadrilateral() { }
      void AddVertex(const Point& p) {
          points[0]=points[1];
          points[1]=points[2];
          points[2]=points[3];
          points[3]=p;
      }

      double Perimeter() const;
  };

  class Rectangle : public Quadrilateral
  {
    public:
      Rectangle() { Quadrilateral();}
      Rectangle(const Point& p1,
                const Point& p2,
                const Point& p3,
                const Point& p4) {Quadrilateral(p1,p2,p3,p4);}
      Rectangle(const Point& p1,
                const double& base,
                const double& height);
      double Perimeter() const { return Quadrilateral::Perimeter();}
      virtual ~Rectangle() { }
  };

  class Square: public Rectangle
  {
    public:
      Square() { Quadrilateral(); }
      Square(const Point& p1,
             const Point& p2,
             const Point& p3,
             const Point& p4) { Quadrilateral(p1,p2,p3,p4);}
      Square(const Point& p1,
             const double& edge) :  Rectangle(p1,edge, edge) {}
      virtual ~Square() { }
      double Perimeter() const { return Rectangle::Perimeter();}
  };
}

#endif // SHAPE_H
