#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

Ellipse::Ellipse()
{
    _a=0.0;
    _b=0.0;
    _center=Point();
}

Ellipse::Ellipse(const Point &center, const double &a, const double &b)
{
    _center=center;
    _a=a;
    _b=b;
}

double Ellipse::Perimeter() const
{
    return 2*M_PI*sqrt((_a*_a+_b*_b)/2);
}

Triangle::Triangle(const Point& p1,
                     const Point& p2,
                     const Point& p3)
  {
    points.resize(3);
    points[0]=p1;
    points[1]=p2;
    points [2]=p3;
  }

  double Triangle::Perimeter() const
  {
    double perimeter = 0;
    perimeter= (points[1]-points[0]).ComputeNorm2()+ (points[2]-points[1]).ComputeNorm2()+ (points[0]-points[2]).ComputeNorm2();
    return perimeter;
  }

  TriangleEquilateral::TriangleEquilateral(const Point& p1,
                                           const double& edge) : Triangle(p1, Point(edge+p1.X ,p1.Y), Point(p1.X+(edge/2),((sqrt(3)/2)*edge)+p1.Y))
  {

  }

  double TriangleEquilateral::perimeter() const
  {
      return Triangle::Perimeter();
  }


  Quadrilateral::Quadrilateral(const Point& p1,
                               const Point& p2,
                               const Point& p3,
                               const Point& p4)
  {
      points.resize(4);
      points[0]=p1;
      points[1]=p2;
      points[2]=p3;
      points[3]=p4;
  }

  double Quadrilateral::Perimeter() const
  {
    double perimeter = 0;
    for (int i=0; i< 3; i++)
    {
        perimeter=perimeter+ (points[i+1]-points[i]).ComputeNorm2();
    }
    perimeter=perimeter+ (points[0]-points[3]).ComputeNorm2();

    return perimeter;
  }

  Rectangle::Rectangle(const Point& p1,
                       const double& base,
                       const double& height) : Quadrilateral(p1,Point(p1.X+base,p1.Y),Point(p1.X+base,p1.Y+height),Point(p1.X,p1.Y+height))
  {

  }

  Point::Point()
  {
      X=0.0;
      Y=0.0;
  }

  Point::Point(const double &x, const double &y)
  {
      X=x;
      Y=y;
  }

  Point::Point(const Point &point)
  {
      *this=point;
  }

  Point Point::operator+(const Point& point) const
  {
      Point tempPoint(X,Y);
      tempPoint.X=X+point.X;
      tempPoint.Y=Y+point.Y;
      return tempPoint;
  }

  Point Point::operator-(const Point& point) const
  {
      Point tempPoint;
      tempPoint.X=X-point.X;
      tempPoint.Y=Y-point.Y;
      return tempPoint;
  }

  Point&Point::operator-=(const Point& point)
  {
      X -=point.X;
      Y -= point.Y;

      return *this;
  }

  Point&Point::operator+=(const Point& point)
  {
      X += point.X; //i+=1 è come i=i+1
      Y += point.Y;
      return *this;
  }


  Circle::Circle(const Point &center, const double &radius) : Ellipse(center,radius,radius)
  {
  }



  double Circle::Perimeter() const
  {
      return Ellipse::Perimeter();
  }

}
