# include "viaMichelin.h"

#include <iostream>
#include <fstream>
#include <sstream>

namespace ViaMichelinLibrary {
int RoutePlanner::BusAverageSpeed = 50;

void BusStation::Load()
{
    _numberBuses=0;
    _bus.clear();

    ifstream file;
    file.open(_busFilePath);
    if (file.fail())
    {
        throw runtime_error("Something goes wrong");
    }
    try {
        string line;
        getline(file,line);
        getline(file,line);
        istringstream convertN;  // converte stringa in intero
        convertN.str(line);
        convertN >> _numberBuses;
        getline(file,line);
        getline (file,line);
        int salta;
        _bus.resize(_numberBuses);
        for (int  b=0; b<_numberBuses; b++)
        {
            getline(file, line);
            istringstream converter;
            converter.str(line);
            converter >> salta >> _bus[b].FuelCost;
            _bus[b].Id=b+1;
        }
    }
    catch (exception) {
        _bus.clear();
        _numberBuses=0;
        throw runtime_error("Something goes wrong");
    }
}

const Bus &BusStation::GetBus(const int &idBus) const
{
    if (idBus > _numberBuses)
      throw runtime_error("Bus " + to_string(idBus) + " does not exists");

    return _bus[idBus - 1];
}

void MapData::reset()
{
    _numberRoutes = 0;
    _numberStreets = 0;
    _numberBusStops = 0;
    _busStops.clear();
    _streets.clear();
    _routes.clear();
    _streetsFrom.clear();
    _streetsTo.clear();
    _routeStreets.clear();

}

void MapData::Load()
{
    reset();
    ifstream file;
    file.open(_mapFilePath);
    if (file.fail())
        throw runtime_error("Something goes wrong");
    try
    {
    string line;
    getline(file,line);
    getline(file, line);
    istringstream  convertN;
    convertN.str(line);
    convertN>>_numberBusStops;
    getline(file,line);

    _busStops.reserve(_numberBusStops);

    for(int b=0; b<_numberBusStops;b++)
    {
        int IdBusStop, latitude, longitude;
        string name;

        getline(file,line);
        istringstream converter;
        converter.str(line);
        converter>>IdBusStop>>name>> latitude>>longitude;
        _busStops.push_back(BusStop());
        BusStop& busStop=_busStops[b];
        busStop.Id=IdBusStop;
        busStop.Name=name;
        busStop.Latitude=latitude;
        busStop.Longitude=longitude;
    }



    getline (file,line);
    getline(file,line);
    istringstream convertStreets;
    convertStreets.str(line);
    convertStreets>>_numberStreets;
    getline(file,line);
    _streets.resize(_numberStreets);
    _streetsFrom.resize(_numberStreets);
    _streetsTo.resize(_numberStreets);
    for(int s=0;s<_numberStreets;s++)
    {
        getline(file,line);
        istringstream convStreets;
        convStreets.str(line);
        convStreets>>_streets[s].Id>>_streetsFrom[s]>>_streetsTo[s]>>_streets[s].TravelTime;
    }



    getline(file,line);
    getline(file,line);
    istringstream convN;
    convN.str(line);
    convN>>_numberRoutes;
    getline(file,line);
    _routes.reserve(_numberRoutes);
    _routeStreets.reserve(_numberRoutes);
    for(int r=0;r<_numberRoutes;r++)
    {
        int idRoute, numberStreets;
        getline(file,line);
        istringstream convertRoutes;
        convertRoutes.str(line);
        convertRoutes>>idRoute>>numberStreets;
        _routes.push_back(Route());
        Route & route=_routes[r];
        route.Id=idRoute;
        route.NumberStreets=numberStreets;
        _routeStreets.push_back(vector<int>());
        _routeStreets[r].reserve(numberStreets);

        for (int s=0; s<numberStreets;s++)
        {
            int idStreet;
            convertRoutes>> idStreet;
            _routeStreets[r].push_back(idStreet);
        }
    }
    file.close();

    }
    catch(exception)
    {
        reset();
        throw runtime_error("Something goes wrong");
    }

}

const Street &MapData::GetRouteStreet(const int &idRoute, const int &streetPosition) const
{
    if(idRoute>_numberRoutes)
        throw runtime_error("Route "+ to_string(idRoute)+ " does not exists");
    const vector<int>& streets=_routeStreets[idRoute-1];
    if (streetPosition >= (int)streets.size() )
      throw runtime_error("Street at position " + to_string(streetPosition) + " does not exists");

    const int & idStreet = streets[streetPosition];

    return GetStreet(idStreet);

}

const Route &MapData::GetRoute(const int &idRoute) const
{
    if (idRoute>_numberRoutes)
        throw runtime_error("Route "+to_string(idRoute)+" does not exists");
    return _routes[idRoute-1];
}

const Street &MapData::GetStreet(const int &idStreet) const
{
    if(idStreet>_numberStreets)
        throw runtime_error("Street "+to_string(idStreet)+" does not exists");
    return  _streets[idStreet-1];
}

const BusStop &MapData::GetStreetFrom(const int &idStreet) const
{
    if (idStreet>_numberStreets)
        throw runtime_error("Street "+to_string(idStreet)+" does not exists");
    const int& idBusStop= _streetsFrom[idStreet-1];
    return GetBusStop(idBusStop);
}

const BusStop &MapData::GetStreetTo(const int &idStreet) const
{
    if  (idStreet>_numberStreets)
        throw runtime_error("Street "+to_string(idStreet)+" does not exists");
    const int& idBusStop=_streetsTo[idStreet-1];
    return GetBusStop(idBusStop);
}

const BusStop &MapData::GetBusStop(const int &idBusStop) const
{
    if(idBusStop>_numberBusStops)
        throw runtime_error("BusStop "+to_string(idBusStop)+" does not exists");
    return  _busStops[idBusStop-1];
}
int RoutePlanner::ComputeRouteTravelTime(const int &idRoute) const
{
    const Route& route = _mapData.GetRoute(idRoute);
    const int& numStreets=route.NumberStreets;
    int travelTime = 0;
    for (int s = 0; s < numStreets; s++)
      {
        const Street &street=_mapData.GetRouteStreet(idRoute,s);
        travelTime+=street.TravelTime;
    }

    return travelTime;
}

int RoutePlanner::ComputeRouteCost(const int &idBus, const int &idRoute) const
{
    const Route& route = _mapData.GetRoute(idRoute);
    const int& numStreets=route.NumberStreets;
    int routeCost = 0;
    const Bus &bus=_busStation.GetBus(idBus);
    for (int s = 0; s < numStreets; s++)
      {
        const Street &street=_mapData.GetRouteStreet(idRoute,s);
        routeCost+=street.TravelTime* bus.FuelCost * BusAverageSpeed;
    }

    return (routeCost/3600);
}



string MapViewer::ViewRoute(const int &idRoute) const
{
    const Route& route = _mapData.GetRoute(idRoute);

    ostringstream routeView;
    routeView<< to_string(idRoute)<< ": ";
    int s=0;
    for (; s < route.NumberStreets - 1; s++)
    {
      int idStreet = _mapData.GetRouteStreet(idRoute, s).Id;
      string from = _mapData.GetStreetFrom(idStreet).Name;
      routeView << from<< " -> ";
    }

    int idStreet = _mapData.GetRouteStreet(idRoute, s).Id;
    string from = _mapData.GetStreetFrom(idStreet).Name;
    string to = _mapData.GetStreetTo(idStreet).Name;
    routeView << from<< " -> "<< to;

    return routeView.str();
}

string MapViewer::ViewStreet(const int &idStreet) const
{
    const BusStop& from = _mapData.GetStreetFrom(idStreet);
    const BusStop& to = _mapData.GetStreetTo(idStreet);

    return to_string(idStreet) + ": " + from.Name + " -> " + to.Name;
}

string MapViewer::ViewBusStop(const int &idBusStop) const
{
    const BusStop& busStop = _mapData.GetBusStop(idBusStop);

    return busStop.Name + " (" + to_string((double)busStop.Latitude / 10000.0) + ", " + to_string((double)busStop.Longitude / 10000.0) + ")";
}






}
