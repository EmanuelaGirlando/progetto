#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

Point::Point(const double &x, const double &y)
{
    _x=x;
    _y=y;
}

Point::Point()
{
    _x=0.0;
    _y=0.0;
}

Point::Point(const Point &point)
{
    _x=point._x;
    _y=point._y;
}

Ellipse::Ellipse(const Point &center, const int &a, const int &b)
{
    _a=a;
    _b=b;
    Center=center;

}
double Ellipse::Area() const
{
    return _a*_b*M_PI;
}

Circle::Circle(const Point &center, const int &radius) : Ellipse (center, radius,radius)
{
}

double Circle::Area() const
{
     return Ellipse::Area();
}

Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3)
{
    _p1=p1;
    _p2=p2;
    _p3=p3;

}

double Triangle::Area() const
{
    double latoA,latoB,latoC;
    latoA=sqrt((_p1._x - _p2._x)*(_p1._x - _p2._x)+(_p1._y - _p2._y)*(_p1._y - _p2._y));
    latoB=sqrt((_p2._x - _p3._x)*(_p2._x - _p3._x)+(_p2._y - _p3._y)*(_p2._y - _p3._y));
    latoC=sqrt((_p3._x - _p1._x)*(_p3._x - _p1._x)+(_p3._y - _p1._y)*(_p3._y - _p1._y));
    double p;
    p=(latoA+latoB+latoC)/2;
    return sqrt(p*(p-latoA)*(p-latoB)*(p-latoC));
}

Quadrilateral::Quadrilateral(const Point &p1, const Point &p2, const Point &p3, const Point &p4)
{
    _p1=p1;
    _p2=p2;
    _p3=p3;
    _p4=p4;
}

double Quadrilateral::Area() const
{
    double area;
    area=abs((_p1._x*_p2._y+_p2._x*_p3._y+_p3._x*_p4._y+_p4._x*_p1._y-_p1._y*_p2._x-_p2._y*_p3._x-_p3._y*_p4._x-_p4._y*_p1._x))*1/2;

    return area;
}

TriangleEquilateral::TriangleEquilateral(const Point &p1, const int &edge) : Triangle(p1, Point(edge+p1._x ,p1._y), Point(p1._x+(edge/2),(sqrt(3)/2)*edge+p1._y))
{
}

double TriangleEquilateral::Area() const
{
    return  Triangle::Area();
}

Parallelogram::Parallelogram(const Point &p1, const Point &p2, const Point &p4) : Quadrilateral(p1,p2,Point(p2._x + p4._x -p1._x,p4._y+p2._y-p1._y),p4)
{
}

double Parallelogram::Area() const
{
    return Quadrilateral::Area();
}

Rectangle::Rectangle(const Point &p1, const int &base, const int &height) : Parallelogram(p1,Point(p1._x+base,p1._y), Point (p1._x,p1._y+height))
{
}

double Rectangle::Area() const
{
    return  Parallelogram::Area();
}

Square::Square(const Point &p1, const int &edge) : Rectangle(p1, edge, edge)
{
}

double Square::Area() const
{
    return Rectangle::Area();
}


}
